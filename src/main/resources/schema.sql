DROP TABLE IF EXISTS users;
create table users
(
    oid        varchar(255) not null primary key,
    created_by varchar(100) not null,
    created_on timestamp    not null,
    is_deleted varchar(5),
    updated_by varchar(100),
    updated_on timestamp,
    email      varchar(255) not null,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    password   varchar(255) not null,
    is_active  varchar(5) default 'Yes':: character varying
);

DROP TABLE IF EXISTS roles;
create table roles
(
    oid         varchar(255) not null primary key,
    created_by  varchar(100) not null,
    created_on  timestamp    not null,
    is_deleted  varchar(5),
    updated_by  varchar(100),
    updated_on  timestamp,
    description text,
    tag         varchar(255) not null,
    title       varchar(255) not null
);

DROP TABLE IF EXISTS user_role;
create table user_role
(
    oid        varchar(255) not null primary key,
    created_by varchar(100) not null,
    created_on timestamp    not null,
    is_deleted varchar(5),
    updated_by varchar(100),
    updated_on timestamp,
    role_oid   varchar(255) not null,
    user_oid   varchar(255) not null,

    foreign key (user_oid) references users (oid),
    foreign key (role_oid) references roles (oid)
);

DROP TABLE IF EXISTS blog_post;
create table blog_post
(
    oid         varchar(255) not null primary key,
    created_by  varchar(100) not null,
    created_on  timestamp    not null,
    is_deleted  varchar(5),
    updated_by  varchar(100),
    updated_on  timestamp,
    description text,
    status      varchar(20) default 'draft':: character varying,
    title       varchar(255) not null
);

DROP TABLE IF EXISTS blog_post_comments;
create table blog_post_comments
(
    oid           varchar(255) not null primary key,
    created_by    varchar(100) not null,
    created_on    timestamp    not null,
    is_deleted    varchar(5),
    updated_by    varchar(100),
    updated_on    timestamp,
    comment       text,
    blog_post_oid varchar(255) not null,
    foreign key (blog_post_oid) references blog_post (oid)
);