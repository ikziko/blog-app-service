-- User entry
INSERT INTO users (oid, first_name, last_name, email, password, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('baa57cbf-107c-43d1-9e0e-05da4415a0ab', 'System', 'Superadmin', 'admin@email.com', '123456', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'No');

INSERT INTO users (oid, first_name, last_name, email, password, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('45808163-4810-4828-9f42-6a3d43e41053', 'Imtiaz', 'Kabir', 'imtiaz@email.com', '123456', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'No');

INSERT INTO users (oid, first_name, last_name, email, password, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('a26574f5-65f4-478c-bda1-a677f67a56e3', 'Mehedi', 'Hasan', 'mehedi@email.com', '123456', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'No');

-- Role entry
INSERT INTO roles (oid, title, tag, description, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('f8df4cb0-fc71-41d1-ae31-c22adf795698', 'Admin', 'admin', '', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.333000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.333000', 'No');

INSERT INTO roles (oid, title, tag, description, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('f8df4cb0-fc71-41d1-ae31-c22adf795699', 'Blogger', 'blogger', '', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'No');

-- User-Role entry
INSERT INTO user_role (oid, user_oid, role_oid, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('f8df4cb0-fc71-41d1-ae31-d22adf795900', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', 'f8df4cb0-fc71-41d1-ae31-c22adf795698', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'No');

INSERT INTO user_role (oid, user_oid, role_oid, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('f8df4cb0-fc71-41d1-ae31-d22adf795901', '45808163-4810-4828-9f42-6a3d43e41053', 'f8df4cb0-fc71-41d1-ae31-c22adf795699', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'No');

INSERT INTO user_role (oid, user_oid, role_oid, created_by, created_on, updated_by, updated_on, is_deleted)
VALUES ('f8df4cb0-fc71-41d1-ae31-d22adf795902', 'a26574f5-65f4-478c-bda1-a677f67a56e3', 'f8df4cb0-fc71-41d1-ae31-c22adf795699', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'baa57cbf-107c-43d1-9e0e-05da4415a0ab', '2021-12-23 13:06:46.322000', 'No');
