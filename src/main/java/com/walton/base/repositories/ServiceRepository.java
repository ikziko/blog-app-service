package com.walton.base.repositories;

import com.walton.base.entities.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@NoRepositoryBean
public interface ServiceRepository<E extends BaseEntity> extends JpaRepository<E, String>, PagingAndSortingRepository<E, String>, JpaSpecificationExecutor<E> {
    Optional<E> findByOidAndIsDeleted(String oid, String isDeleted);
    List<E> findByOidInAndIsDeleted(Set<String> oids, String isDeleted);
    Page<E> findByIsDeleted(String isDeleted, Pageable pageable);
    Page<E> findByOidInAndIsDeleted(Set<String> oids, String isDeleted, Pageable pageable);
}