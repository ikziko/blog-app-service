package com.walton.base.helpers;

public interface BaseConstants {

    String YES = "Yes";
    String NO = "No";
    String SORT_ASC = "asc";
    String SORT_DESC = "desc";

    String INVALID_TOKEN = "Invalid token.";

}
