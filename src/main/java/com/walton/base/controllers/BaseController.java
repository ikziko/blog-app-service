package com.walton.base.controllers;


import com.walton.helpers.components.RequestContextComponent;
import com.walton.helpers.components.ResultRetrieverComponent;
import com.walton.helpers.components.TokenDecodingComponent;
import com.walton.helpers.dataclass.common.HeaderDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@CrossOrigin
public abstract class BaseController {

    @Autowired
    TokenDecodingComponent tokenDecodingComponent;

    @Autowired
    HttpServletRequest request;

    @Autowired
    private RequestContextComponent requestContextComponent;

    // TODO: Try to log using Interceptors if possible
    protected <R extends ServiceResponseDTO, B> R send(
            String url,
            ServiceRequestDTO<B> requestDTO,
            ResultRetrieverComponent<R, B> retriever
    ) {
        HeaderDTO headerDTO = tokenDecodingComponent.decode(request.getHeader("Authorization"));

        log.info("RequestSource {}", url);
        log.info("RequestBody {}", requestDTO);
        log.info(" Token {}" , request.getHeader("Authorization"));

        requestDTO.getHeader().setRequestUrl(url);

        requestContextComponent.setRequestDTO(requestDTO);
        requestContextComponent.setHeaderDTO(headerDTO);

        R responseDTO = retriever.retrieve(requestDTO,headerDTO);


        log.info("RequestSource {}", url);
        log.warn(" Testing ResponseBody {}", responseDTO);

        return responseDTO;
    }
}
