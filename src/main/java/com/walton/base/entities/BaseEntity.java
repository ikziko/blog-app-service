package com.walton.base.entities;

import com.walton.base.helpers.BaseConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;

@Component
@MappedSuperclass
@Data
public class BaseEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String oid;

    @Column(name = "created_by", nullable = false, updatable = false, columnDefinition="varchar(100)")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on", nullable = false, updatable = false)
    @CreatedDate
    @JsonIgnore
    private Date createdOn;

    @Column(name = "updated_by", columnDefinition="varchar(100)")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    @LastModifiedDate
    @JsonIgnore
    private Date updatedOn;

    @Column(name = "is_deleted", columnDefinition="varchar(5)")
    private String isDeleted = BaseConstants.NO;

}