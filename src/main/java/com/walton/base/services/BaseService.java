package com.walton.base.services;

import com.walton.base.entities.BaseEntity;
import com.walton.base.helpers.BaseConstants;
import com.walton.base.repositories.ServiceRepository;
import com.walton.helpers.components.EventManagementComponent;
import com.walton.helpers.components.HeaderUtilComponent;
import com.walton.helpers.constants.Constants;
import com.walton.helpers.constants.CrudEvent;
import com.walton.helpers.dataclass.common.EmptyBodyDTO;
import com.walton.helpers.dataclass.common.HeaderDTO;
import com.walton.helpers.dataclass.common.SearchDTO;
import com.walton.helpers.dataclass.request.*;
import com.walton.helpers.dataclass.response.ServiceResponseBodyDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import com.walton.helpers.exceptions.ServiceExceptionHolder;
import com.walton.helpers.models.MetaModel;
import com.walton.helpers.models.SortModel;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.util.Pair;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Data
public abstract class BaseService<E extends BaseEntity, D extends IOidHolderRequestBodyDTO> {

    private final HeaderUtilComponent headerUtilComponent;
    private final ServiceRepository<E> repository;
    private final ModelMapper modelMapper;
    private final EventManagementComponent eventManagementComponent;

    protected <T> ServiceResponseDTO<T> generateResponse(ServiceRequestHeaderDTO header, List<T> data) {
        return new ServiceResponseDTO<>(headerUtilComponent.getResponseHeaderDTO(header), new MetaModel(), new ServiceResponseBodyDTO<>(data));
    }

    protected <T> ServiceResponseDTO<T> generateResponse(ServiceRequestHeaderDTO header, MetaModel meta, List<T> data) {
        return new ServiceResponseDTO<>(headerUtilComponent.getResponseHeaderDTO(header), meta, new ServiceResponseBodyDTO<>(data));
    }

    public ServiceResponseDTO<D> getByOid(ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO, HeaderDTO headerDTO) {
        return generateResponse(requestDTO.getHeader(), convertForRead(Collections.singletonList(getByOid(requestDTO.getBody().getData().get(0).getOid()))));
    }

    public ServiceResponseDTO<D> getList(ServiceRequestDTO<EmptyBodyDTO> requestDTO, HeaderDTO headerDTO) {
        Page<E> page = repository.findByIsDeleted(BaseConstants.NO, getPageable(requestDTO.getMeta()));
        return generateResponse(requestDTO.getHeader(), getMeta(requestDTO.getMeta(), page), convertForRead(page.hasContent() ? page.getContent() : new ArrayList<>()));
    }

    protected List<E> getListOfEntitiesByOidSet(Set<String> oids) {
        return repository.findAllById(oids)
                .stream()
                .filter(e -> e.getIsDeleted().equals(BaseConstants.NO))
                .collect(Collectors.toList());
    }

    protected Page<E> getListOfEntitiesByOidSet(Set<String> oids, MetaModel meta) {
        return repository.findByOidInAndIsDeleted(oids, BaseConstants.NO, getPageable(meta));
    }

    public ServiceResponseDTO<D> getListByOidSet(ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO, HeaderDTO headerDTO) {
        GetListByOidSetRequestBodyDTO body = requestDTO.getBody().getData().get(0);
        Set<String> oids = body.getOids();
        Page<E> page = getListOfEntitiesByOidSet(oids, requestDTO.getMeta());
        List<E> result = page.hasContent() ? page.getContent() : new ArrayList<>();
        handleStrictness(oids, result, body.getStrict());
        return generateResponse(requestDTO.getHeader(), getMeta(requestDTO.getMeta(), page), convertForRead(result));
    }

    public void handleStrictness(Set<String> oids, List<E> eList, String strict) {
        if (strict != null && strict.equals(BaseConstants.YES) && eList.size() < oids.size()) {
            Map<String, Boolean> oidMap = new HashMap<>();
            List<String> missingList = new ArrayList<>();

            oids.forEach(s -> oidMap.put(s, false));
            eList.forEach(e -> oidMap.put(e.getOid(), true));
            oidMap.keySet().stream().filter(s -> !oidMap.get(s)).forEach(missingList::add);
            throw new ServiceExceptionHolder.ResourceNotFoundException(
                    "No "
                            + String.join(" ", getEntityClass().getSimpleName().split("(?=\\p{Upper})"))
                            + " Found for IDs : " + String.join(",", missingList)
            );
        }
    }

    public ServiceResponseDTO<D> createAll(ServiceRequestDTO<D> requestDTO, HeaderDTO headerDTO) {
        return generateResponse(requestDTO.getHeader(), createAll(requestDTO.getBody().getData(), headerDTO));
    }

    protected boolean isValid(D dto) {
        return true;
    }

    protected void postCreateAll(List<D> dtos, List<E> entities) {

    }

    protected void postUpdateAll(List<D> dtos, List<E> entities) {

    }

    public List<D> createAll(List<D> dtos, HeaderDTO headerDTO) {

        List<E> entities = new ArrayList<>();
        E e;
        for (D dto : dtos) {
            if (!isValid(dto)) continue;
            e = convertForCreate(dto);
            e.setCreatedBy(headerDTO.getUserOid());
            e.setCreatedOn(new Date());
            e.setIsDeleted(BaseConstants.NO);
            entities.add(e);
        }
        List<E> createdEntities = repository.saveAll(entities);
        postCreateAll(dtos, createdEntities);
        List<String> oids = createdEntities.stream().map(o -> o.getOid()).collect(Collectors.toList());
        eventManagementComponent.publishCrudEventBulk(Pair.of(getEntityClass(), CrudEvent.CREATE), oids);
        return createdEntities.stream().map(o -> convertForRead(o)).collect(Collectors.toList());
    }

    public ServiceResponseDTO<D> updateAll(ServiceRequestDTO<D> requestDTO, HeaderDTO headerDTO) {
        return generateResponse(requestDTO.getHeader(), updateAll(requestDTO.getBody().getData(), headerDTO));
    }

    public List<D> updateAll(List<D> dtos, HeaderDTO headerDTO) {
        List<E> updatedEntities = new ArrayList<>();
        E e;
        for (D dto : dtos) {
            if (dto.getOid() == null)
                throw new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException("No Oid Provided for " + getEntityClass().getSimpleName());
            e = getByOidForWrite(dto.getOid());
            e = convertForUpdate(dto, e);
            e.setUpdatedBy(headerDTO.getUserOid());
            e.setUpdatedOn(new Date());
            e.setIsDeleted(BaseConstants.NO);
            updatedEntities.add(e);
        }
        updatedEntities = repository.saveAll(updatedEntities);
        postUpdateAll(dtos, updatedEntities);
        List<String> oids = updatedEntities.stream().map(o -> o.getOid()).collect(Collectors.toList());
        eventManagementComponent.publishCrudEventBulk(Pair.of(getEntityClass(), CrudEvent.UPDATE), oids);
        return updatedEntities.stream().map(o -> convertForRead(o)).collect(Collectors.toList());
    }

    public ServiceResponseDTO<D> deleteAll(ServiceRequestDTO<String> requestDTO, HeaderDTO headerDTO) {
        Set<String> oidList = new HashSet<String>(requestDTO.getBody().getData());
        return generateResponse(requestDTO.getHeader(), deleteAll(oidList, headerDTO));
    }

    public List<D> deleteAll(Set<String> oidList, HeaderDTO headerDTO) {
        List<D> deletedEntities = oidList.stream().map(o -> deleteEntity(getByOidForWrite(o), headerDTO)).collect(Collectors.toList());
        return deletedEntities;
    }


    protected D deleteEntity(E e, HeaderDTO headerDTO) {
        eventManagementComponent.publishCrudEvent(Pair.of(getEntityClass(), CrudEvent.DELETE), e.getOid());
        e.setUpdatedBy(headerDTO.getUserOid());
        e.setUpdatedOn(new Date());
        e.setIsDeleted(BaseConstants.YES);
        repository.save(e);
        return convertForRead(e);

    }

    protected E getByOid(@NonNull String oid) {
        return getOptionalEntity(oid).orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundException(
                "No " + getEntityClass().getSimpleName() + " Found with ID: " + oid));
    }

    protected E getByOidForWrite(@NonNull String oid) {
        return getOptionalEntity(oid)
                .orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException(
                        "No " + getEntityClass().getSimpleName() + " Found with ID: " + oid));
    }

    public Optional<E> getOptionalEntity(@NonNull String oid) {
        return repository.findByOidAndIsDeleted(oid, BaseConstants.NO);
    }

    protected D convertForRead(E e) {
        return modelMapper.map(e, getDtoClass());
    }

    protected D convertForRead(E e, HeaderDTO headerDTO) {
        return modelMapper.map(e, getDtoClass());
    }

    protected E convertForCreate(D d) {
        E e = null;
        try {
            e = getEntityClass().newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
        BeanUtils.copyProperties(d, e);
        return e;
    }

    protected E convertForUpdate(D d, E e) {
        BeanUtils.copyProperties(d, e);
        return e;
    }

    protected List<D> convertForRead(List<E> e) {
        return e.stream().map(this::convertForRead).collect(Collectors.toList());
    }

    protected List<D> convertForRead(List<E> elist, HeaderDTO headerDTO) {
        return elist.stream().map(e -> convertForRead(e, headerDTO)).collect(Collectors.toList());
    }


    @SuppressWarnings("unchecked")
    private Class<E> getEntityClass() {
        return (Class<E>) (((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
    }

    @SuppressWarnings("unchecked")
    private Class<D> getDtoClass() {
        return (Class<D>) (((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1]);
    }

    public Set<String> validateSet(Set<String> set) {
        if (set == null || set.size() == 0) {
            set.add("");
        }
        return set;
    }

    public List<E> createAllEntity(List<E> entities, HeaderDTO headerDTO) {

        for (E e : entities) {
            System.out.println("e = " + e);
            e.setCreatedBy(headerDTO.getUserOid());
            e.setCreatedOn(new Date());
            e.setIsDeleted(BaseConstants.NO);
        }
        List<E> createdEntities = null;
        try {
            System.out.println("entities = " + entities);
            createdEntities = repository.saveAll(entities);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> oids = createdEntities.stream().map(o -> o.getOid()).collect(Collectors.toList());

        eventManagementComponent.publishCrudEventBulk(Pair.of(getEntityClass(), CrudEvent.CREATE), oids);

        return createdEntities;
    }

    public List<E> updateAllEntity(List<E> entities, HeaderDTO headerDTO) {

        for (E e : entities) {
            e.setUpdatedBy(headerDTO.getUserOid());
            e.setUpdatedOn(new Date());
            e.setIsDeleted(BaseConstants.NO);
        }
        List<E> updatedEntities = repository.saveAll(entities);

        List<String> oids = updatedEntities.stream().map(o -> o.getOid()).collect(Collectors.toList());
        eventManagementComponent.publishCrudEventBulk(Pair.of(getEntityClass(), CrudEvent.UPDATE), oids);

        return updatedEntities;
    }

    public List<E> deleteAllEntity(List<String> oidList, HeaderDTO headerDTO) {
        List<E> entities = oidList.stream().map(o -> getByOidForWrite(o)).collect(Collectors.toList());
        return deleteAll(entities, headerDTO);
    }

    public List<E> deleteAll(List<E> entities, HeaderDTO headerDTO) {

        for (E e : entities) {
            e.setUpdatedBy(headerDTO.getUserOid());
            e.setUpdatedOn(new Date());
            e.setIsDeleted(BaseConstants.YES);
        }
        List<E> updatedEntities = repository.saveAll(entities);

        List<String> oids = updatedEntities.stream().map(o -> o.getOid()).collect(Collectors.toList());
        eventManagementComponent.publishCrudEventBulk(Pair.of(getEntityClass(), CrudEvent.DELETE), oids);

        return updatedEntities;
    }


    public ServiceResponseDTO<D> search(ServiceRequestDTO<SearchDTO> requestDTO, HeaderDTO headerDTO) {
        boolean hasMetaData = requestDTO.getMeta() != null && requestDTO.getMeta().getLimit() != null && requestDTO.getMeta().getOffset() != null;
        SearchDTO filter = (null != requestDTO.getBody().getData() && requestDTO.getBody().getData().size() > 0) ? (requestDTO.getBody().getData().get(0)) : new SearchDTO();
        if (hasMetaData) {
            Page<E> page = repository.findAll(findByCriteria(filter), getPageable(requestDTO.getMeta()));
            return generateResponse(requestDTO.getHeader(), getMeta(requestDTO.getMeta(), page), convertForRead(page.getContent()));
        }
        List<E> results = repository.findAll(findByCriteria(filter));
        return generateResponse(requestDTO.getHeader(), convertForRead(results));
    }

    protected Specification<E> findByCriteria(SearchDTO dto) {
        return new Specification<E>() {
            @Override
            public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                // return must active row
                predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("isDeleted"), Constants.NO)));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };
    }

    public MetaModel getMeta(MetaModel meta, Page page) {
        if (page.hasContent()) {
            meta.setCount(page.getTotalElements());
            meta.setTotalPageCount(page.getTotalPages());
            meta.setResultCount(page.getNumberOfElements());
        }
        Integer currentOffset = meta.getOffset();
        if (null != currentOffset) {
            Integer prevOffset = currentOffset - 1;
            Integer nextOffset = currentOffset + 1;
            if (prevOffset < 0) prevOffset = 0;
            if (nextOffset == page.getTotalPages()) nextOffset -= 1;
            meta.setPrevOffset(prevOffset);
            meta.setNextOffset(nextOffset);
        }
        return meta;
    }

    public Pageable getPageable(MetaModel meta) {
        if (meta == null || meta.getOffset() == null || meta.getLimit() == null) return null;
        // has sorted properties inside meta
        if (null != meta.getSort() && meta.getSort().size() > 0)
            return PageRequest.of(meta.getOffset(), meta.getLimit(), Sort.by(getSortOrders(meta.getSort())));
        // has no sorted properties inside meta
        return PageRequest.of(meta.getOffset(), meta.getLimit());
    }

    private List<Sort.Order> getSortOrders(List<SortModel> sortModels) {
        List<Sort.Order> orders = new ArrayList<>();
        if (null != sortModels && sortModels.size() > 0)
            sortModels.stream().forEach(model -> {
                if(null!=model.getField() && null!=model.getOrder()
                        && !model.getField().isEmpty() && !model.getOrder().isEmpty()){
                    orders.add(new Sort.Order(getDirection(model.getOrder()), model.getField()));
                }
            });
        orders.add(new Sort.Order(Sort.Direction.DESC, "createdOn"));
        return orders;
    }

    private Sort.Direction getDirection(String order) {
        if (null != order && order.equals(BaseConstants.SORT_DESC)) return Sort.Direction.DESC;
        return Sort.Direction.ASC;
    }


}
