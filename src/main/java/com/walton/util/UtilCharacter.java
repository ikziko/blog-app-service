package com.walton.util;

import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class UtilCharacter {

    public String convertNumberEnToBn(String txt) {
        if (txt == null) return null;
        String replacedVersion = txt
                .replaceAll("0", "০").replaceAll("1", "১")
                .replaceAll("2", "২").replaceAll("3", "৩")
                .replaceAll("4", "৪").replaceAll("5", "৫")
                .replaceAll("6", "৬").replaceAll("7", "৭")
                .replaceAll("8", "৮").replaceAll("9", "৯");
        return replacedVersion;
    }

    public String convertNumberBnToEn(String txt) {
        if (txt == null) return null;
        String replacedVersion = txt
                .replaceAll("০", "0").replaceAll("১", "1")
                .replaceAll("২", "2").replaceAll("৩", "3")
                .replaceAll("৪", "4").replaceAll("৫", "5")
                .replaceAll("৬", "6").replaceAll("৭", "7")
                .replaceAll("৮", "8").replaceAll("৯", "9");
        return replacedVersion;
    }

    public boolean noData(String txt) {
        //return txt == null || txt.trim().equals("");
        boolean flag = false;
        if (null == txt || txt.trim().isEmpty()) {
            flag = true;
        }
        return flag;
    }

    public boolean noData(Object obj) {
        return obj == null;
    }

    public boolean noData(Collection<?> c) {
        return c == null || c.isEmpty();
    }

    public String calculateTotalHourForBn(String inTime, String outTime) {
        inTime = convertNumberBnToEn(inTime.trim());
        outTime = convertNumberBnToEn(outTime.trim());

        String[] t = inTime.split(":");
        int inTimeInt = Integer.parseInt(t[0]) * 60 + Integer.parseInt(t[1]);

        t = outTime.split(":");
        int outTimeint = Integer.parseInt(t[0]) * 60 + Integer.parseInt(t[1]);

        int totalHourMinute = outTimeint - inTimeInt;
        if (inTimeInt > outTimeint) { //12*60 to compare AM/PM
            if (inTimeInt > 720) {
                outTimeint = outTimeint + 1440;
            } else {
                outTimeint = outTimeint + 720;
            }
            totalHourMinute = outTimeint - inTimeInt;
        }
        return getHourMinuteFromTxt(totalHourMinute);
    }

    public String getHourMinuteFromTxt(int totalHourMinute) {
        int minute = totalHourMinute % 60;
        int hour = totalHourMinute / 60;
        String txt = "";
        String preHour = "";
        String preMinute = "";
        if (String.valueOf(hour).length() == 1) {
            preHour = "0";
        }
        if (String.valueOf(hour).length() == 0) {
            preHour = "00";
        }
        if (String.valueOf(minute).length() == 1) {
            preMinute = "0";
        }
        if (String.valueOf(minute).length() == 0) {
            preMinute = "00";
        }
        return convertNumberEnToBn(preHour + hour + ":" + preMinute + minute);
    }

    public int hourPlus(int hourOld, String hourNew) {
        //hourOld = hourOld * 60; //minute
        hourNew = convertNumberBnToEn(hourNew);
        if (hourNew.contains(":")) {
            String[] t = hourNew.split(":");
            int a = Integer.parseInt(t[0]);
            a = a * 60;
            int b = Integer.parseInt(t[1]);
            hourOld = hourOld + a + b;
        } else {
            int a = Integer.parseInt(hourNew);
            a = a * 60; //min
            hourOld = hourOld + a;
        }
        return hourOld;
    }

}