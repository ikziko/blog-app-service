package com.walton.helpers.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class DecimalSerializer<T> extends JsonSerializer<T> {

    @Override
    public void serialize(T t, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {

        if (null == t) generator.writeNumber(BigDecimal.ZERO.toPlainString());
        if (t instanceof BigDecimal)
            generator.writeNumber(new BigDecimal(((BigDecimal) t).toPlainString()).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString());
        if (t instanceof Double)
            generator.writeNumber(new BigDecimal((Double) t).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString());
        if (t instanceof Float)
            generator.writeNumber(new BigDecimal((Float) t).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString());
    }

}