package com.walton.helpers.components;


import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import com.walton.helpers.dataclass.common.HeaderDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;

public interface ResultRetrieverComponent<R extends ServiceResponseDTO, B> {

    R retrieve(ServiceRequestDTO<B> requestDTO, HeaderDTO headerDTO);

}
