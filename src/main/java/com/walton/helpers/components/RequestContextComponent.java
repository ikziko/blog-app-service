package com.walton.helpers.components;


import com.walton.helpers.dataclass.common.HeaderDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
@Data
public class RequestContextComponent {

    private ServiceRequestDTO requestDTO;

    private HeaderDTO headerDTO;

}