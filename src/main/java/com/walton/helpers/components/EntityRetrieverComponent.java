package com.walton.helpers.components;


import com.walton.helpers.exceptions.ServiceExceptionHolder;
import com.walton.base.entities.BaseEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class EntityRetrieverComponent {

    private final RepositoryFactoryComponent repositoryFactoryComponent;

    public <E extends BaseEntity> E getByOidForRead(Class<E> cls, String oid) {
        return getOptionalEntity(cls, oid)
                .orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundException(
                        getMessage(oid, cls)));
    }

    public <E extends BaseEntity> E getByOidForWrite(Class<E> cls, String oid) {
        return getOptionalEntity(cls, oid)
                .orElseThrow(() -> new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException(
                        getMessage(oid, cls)));
    }

    private <E extends BaseEntity> Optional<E> getOptionalEntity(Class<E> cls, String oid) {
        return repositoryFactoryComponent.getRepository(cls)
                .findByOidAndIsDeleted(oid, "No");
    }

    private String getMessage(String oid, Class<?> cls) {
        return "No " + String.join(" ", cls.getSimpleName().split("(?=\\p{Upper})"))
                + " Found with ID : " + oid;
    }

}
