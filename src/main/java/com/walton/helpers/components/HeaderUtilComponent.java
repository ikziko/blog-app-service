package com.walton.helpers.components;


import com.walton.helpers.exceptions.ServiceExceptionHandlers;
import com.walton.config.ServiceConfiguration;
import com.walton.helpers.dataclass.error.ServiceErrorResponseHeaderDTO;
import com.walton.helpers.dataclass.request.ServiceRequestHeaderDTO;
import com.walton.helpers.dataclass.response.ServiceResponseHeaderDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class HeaderUtilComponent {

    private final ServiceConfiguration serviceConfiguration;

    public ServiceResponseHeaderDTO getResponseHeaderDTO(ServiceRequestHeaderDTO requestHeaderDTO) {
        ServiceResponseHeaderDTO responseHeaderDTO = new ServiceResponseHeaderDTO();
        responseHeaderDTO.setRequestReceivedTime(requestHeaderDTO.getRequestTime());
        responseHeaderDTO.setResponseTime(new Date());
        responseHeaderDTO.setHopCount(requestHeaderDTO.getHopCount() == null ? 1 : requestHeaderDTO.getHopCount() + 1);
        responseHeaderDTO.setRequestId(requestHeaderDTO.getRequestId());
        responseHeaderDTO.setResponseCode(String.valueOf(HttpStatus.OK.value()));
        responseHeaderDTO.setResponseMessage("Successfully performed");
        responseHeaderDTO.setResponseVersion(serviceConfiguration.getVersion());
        responseHeaderDTO.setRequestSourceService(requestHeaderDTO.getRequestSourceService());
        responseHeaderDTO.setTraceId((requestHeaderDTO.getTraceId() == null || requestHeaderDTO.getTraceId().isEmpty()) ? UUID.randomUUID().toString() : requestHeaderDTO.getTraceId());
        return responseHeaderDTO;
    }

    public ServiceErrorResponseHeaderDTO getErrorResponseHeaderDTO(ServiceRequestHeaderDTO requestHeaderDTO) {
        ServiceErrorResponseHeaderDTO responseHeaderDTO = new ServiceErrorResponseHeaderDTO();
        responseHeaderDTO.setRequestReceivedTime(requestHeaderDTO.getRequestTime());
        responseHeaderDTO.setResponseTime(new Date());
        responseHeaderDTO.setHopCount(requestHeaderDTO.getHopCount() == null ? 1 : requestHeaderDTO.getHopCount() + 1);
        responseHeaderDTO.setRequestId(requestHeaderDTO.getRequestId());
        responseHeaderDTO.setResponseCode(String.valueOf(HttpStatus.OK.value()));
        responseHeaderDTO.setResponseMessage("Successfully performed");
        responseHeaderDTO.setResponseVersion(serviceConfiguration.getVersion());
        responseHeaderDTO.setRequestSourceService(requestHeaderDTO.getRequestSourceService());
        responseHeaderDTO.setTraceId((requestHeaderDTO.getTraceId() == null || requestHeaderDTO.getTraceId().isEmpty()) ? UUID.randomUUID().toString() : requestHeaderDTO.getTraceId());
        return responseHeaderDTO;
    }

    public ServiceErrorResponseHeaderDTO getResponseHeaderDTO(ServiceExceptionHandlers.ApiErrorResponse apiErrorResponse) {
        ServiceErrorResponseHeaderDTO responseHeaderDTO = new ServiceErrorResponseHeaderDTO();
        responseHeaderDTO.setRequestReceivedTime(new Date());
        responseHeaderDTO.setResponseTime(new Date());
        responseHeaderDTO.setHopCount(1);
        responseHeaderDTO.setRequestId(UUID.randomUUID().toString());
        responseHeaderDTO.setResponseCode(apiErrorResponse.getCode());
        responseHeaderDTO.setResponseMessage(apiErrorResponse.getMessage());
        responseHeaderDTO.setResponseVersion(serviceConfiguration.getVersion());
        responseHeaderDTO.setTraceId(UUID.randomUUID().toString());
        return responseHeaderDTO;
    }

    public ServiceRequestHeaderDTO getRequestHeaderDTO() {
        ServiceRequestHeaderDTO requestHeaderDTO = new ServiceRequestHeaderDTO();
        requestHeaderDTO.setRequestId("random-uuid");
        requestHeaderDTO.setRequestSource("portal");
        requestHeaderDTO.setRequestSourceService("portal");
        requestHeaderDTO.setRequestClient("");
        requestHeaderDTO.setRequestType("random");
        requestHeaderDTO.setRequestTime(new Date());
        requestHeaderDTO.setRequestVersion("v1");
        requestHeaderDTO.setRequestTimeoutInSeconds(30);
        requestHeaderDTO.setRequestRetryCount(3);
        return requestHeaderDTO;
    }

}