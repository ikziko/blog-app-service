package com.walton.helpers.components;


import com.walton.base.entities.BaseEntity;
import com.walton.helpers.constants.CrudEvent;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class EventManagementComponent {

    private final Map<Pair<Class<? extends BaseEntity>, CrudEvent>, Set<CrudEventProcessor>> processorRegistrar;

    public EventManagementComponent() {
        processorRegistrar = new HashMap<>();
    }

    public void registerProcess(Pair<Class<? extends BaseEntity>, CrudEvent> event, CrudEventProcessor processor) {
        processorRegistrar.computeIfAbsent(event, k -> new HashSet<>());
        processorRegistrar.get(event).add(processor);
    }

    public void publishCrudEvent(Pair<Class<? extends BaseEntity>, CrudEvent> event, String oid) {
        Set<CrudEventProcessor> processors = processorRegistrar.get(event);
        if (processors != null) processors.forEach(o -> o.process(event, oid));
    }

    public void publishCrudEventBulk(Pair<Class<? extends BaseEntity>,CrudEvent> event, List<String> oids){
        Set<CrudEventProcessor> processors = processorRegistrar.get(event);
        if (processors != null) processors.forEach(o -> o.processBulk(event,oids));

    }
}
