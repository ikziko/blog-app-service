package com.walton.helpers.components;


import com.walton.base.entities.BaseEntity;
import com.walton.helpers.constants.CrudEvent;
import org.springframework.data.util.Pair;

import java.util.List;

public interface CrudEventProcessor {
    void process(Pair<Class<? extends BaseEntity>, CrudEvent> event, String oid);

    void processBulk(Pair<Class<? extends BaseEntity>, CrudEvent> event, List<String> oid);
}
