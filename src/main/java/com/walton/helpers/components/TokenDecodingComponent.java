package com.walton.helpers.components;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.walton.base.helpers.BaseConstants;
import com.walton.helpers.dataclass.common.HeaderDTO;
import com.walton.helpers.exceptions.ServiceExceptionHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Component;

@Component
public class TokenDecodingComponent {

    @Autowired
    ObjectMapper objectMapper;

    public HeaderDTO decode(String token) {
        HeaderDTO headerDTO = null;
        if (token == null) throw new ServiceExceptionHolder.TokenValidationException(BaseConstants.INVALID_TOKEN);
        if (token.startsWith("Bearer ")) token = token.substring(7);
        try {
            Jwt jwtToken = JwtHelper.decode(token);
            String body = jwtToken.getClaims();
            headerDTO = objectMapper.readValue(body, HeaderDTO.class);
            headerDTO.setToken(token);
        } catch (Exception e) {
            throw new ServiceExceptionHolder.TokenValidationException(BaseConstants.INVALID_TOKEN);
        }
        return headerDTO;
    }


}
