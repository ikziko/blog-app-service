package com.walton.helpers.validators.annotations;

import com.walton.helpers.validators.implementations.EntityOidValidityCheckerExtended;
import com.walton.base.entities.BaseEntity;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = EntityOidValidityCheckerExtended.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEntityOid {

    Class<? extends BaseEntity> value();
    String message() default "invalid oid provided";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}