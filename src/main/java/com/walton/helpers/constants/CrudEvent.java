package com.walton.helpers.constants;

public enum CrudEvent {
    CREATE,
    UPDATE,
    DELETE
}