package com.walton.helpers.constants;

public interface Api {

    String BASE = "";

    // controller root
    String ENDPOINT_AUTH = BASE + "/auth";
    String ENDPOINT_ROLE = BASE + "/roles";
    String ENDPOINT_USERS = BASE + "/users";
    String ENDPOINT_USER_ROLE = BASE + "/user-role";
    String ENDPOINT_BLOG_POST = BASE + "/blog-post";
    String ENDPOINT_BLOG_POST_COMMENT = BASE + "/blog-post-comment";

    String SEARCH_PATH = "/search";
    String CREATE_ALL_PATH = "/create-all";
    String CREATE_PATH = "/create";
    String UPDATE_ALL_PATH = "/update-all";
    String UPDATE_PATH = "/update";
    String DELETE_ALL_PATH = "/delete-all";
    String GET_BY_OID_PATH = "/get-by-oid";
    String GET_LIST_PATH = "/get-list";
    String GET_LIST_BY_OID_SET_PATH = "/get-list-by-oid-set";


    String ACTIVATE_OR_DEACTIVATE = "/activate-or-deactivate";
    String GET_DRAFTED_POST_LIST_PATH = "/get-drafted-post-list";
    String GET_PUBLISHED_POST_LIST_PATH = "/get-published-post-list";
    String GET_APPROVED_POST_LIST_PATH = "/get-approved-post-list";
    String PUBLISH_POST_PATH = "/publish-post";
    String APPROVE_POST_PATH = "/approve-post";


}
