package com.walton.helpers.constants;

import com.walton.base.helpers.BaseConstants;

public interface Constants extends BaseConstants {
    String STATUS_DRAFT = "draft";
    String STATUS_PUBLISHED = "published";
    String STATUS_APPROVED = "approved";
}