package com.walton.helpers.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

public class ServiceExceptionHolder {

    @Getter
    @RequiredArgsConstructor
    public static class ServiceException extends RuntimeException {
        private final int code;
        private final String message;
    }

    public static class ResourceNotFoundException extends ServiceException {
        public ResourceNotFoundException(String message) {
            super(400, message);
        }
    }

    public static class ResourceAlreadyExistsException extends ServiceException {
        public ResourceAlreadyExistsException(String message) {
            super(400, message);
        }
    }

    public static class ResourceInValidException extends ServiceException {
        public ResourceInValidException(String message) {
            super(400, message);
        }
    }

    public static class ResourceNotFoundDuringWriteRequestException extends ServiceException {
        public ResourceNotFoundDuringWriteRequestException(String message) {
            super(400, message);
        }
    }

    public static class IllegalDateFormatException extends ServiceException {
        public IllegalDateFormatException(String message) {
            super(HttpStatus.BAD_REQUEST.value(), message);
        }
    }

    public static class TokenValidationException extends ServiceException {
        public TokenValidationException(String message) {
            super(400, message);
        }
    }

    public static class BadRequestException extends ServiceException {
        public BadRequestException() {
            super(HttpStatus.BAD_REQUEST.value(), "দুঃখিত, অনুরোধটি সম্পন্ন করা যায়নি");
        }

        public BadRequestException(String message) {
            super(HttpStatus.BAD_REQUEST.value(), message);
        }
    }

}