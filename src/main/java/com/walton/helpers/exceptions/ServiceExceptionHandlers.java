package com.walton.helpers.exceptions;

import com.walton.config.ServiceConfiguration;
import com.walton.helpers.components.HeaderUtilComponent;
import com.walton.helpers.components.RequestContextComponent;
import com.walton.helpers.dataclass.error.ServiceErrorModel;
import com.walton.helpers.dataclass.error.ServiceErrorResponseDTO;
import com.walton.helpers.dataclass.error.ServiceErrorResponseHeaderDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseBodyDTO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.*;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ServiceExceptionHandlers {

    @Autowired
    private RequestContextComponent requestContextComponent;

    private final ServiceConfiguration serviceConfiguration;
    private final HeaderUtilComponent headerUtilComponent;

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(ServiceExceptionHolder.ResourceNotFoundException.class)
    public ServiceErrorResponseDTO handleUserNotFoundException(final ServiceExceptionHolder.ResourceNotFoundException ex) {

        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException.class)
    public ServiceErrorResponseDTO handleUserNotFoundException(final ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException ex) {
    	return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.ResourceAlreadyExistsException.class)
    public ServiceErrorResponseDTO handleAlreadyExistsException(final ServiceExceptionHolder.ResourceAlreadyExistsException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.BadRequestException.class)
    public ServiceErrorResponseDTO handleCustomizedBadRequestException(final ServiceExceptionHolder.BadRequestException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.IllegalDateFormatException.class)
    public ServiceErrorResponseDTO handleIllegalDateFormatException(final ServiceExceptionHolder.IllegalDateFormatException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.TokenValidationException.class)
    public ServiceErrorResponseDTO handleTokenValidationException(final ServiceExceptionHolder.TokenValidationException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }


    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ServiceErrorResponseDTO handleUnsupportedMediaType(final HttpMediaTypeNotSupportedException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getLocalizedMessage()));
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ServiceErrorResponseDTO handleValidationError(final HttpMessageNotReadableException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse("Could not read the request body"));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ServiceErrorResponseDTO handleInvalidRequestParam(final MethodArgumentTypeMismatchException ex) {
        return getProcessedApiErrorResponse(new ApiErrorResponse("Invalid Request Parameters"));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ServiceErrorResponseDTO handleValidationError(final MethodArgumentNotValidException ex) {
        Errors validationErrors = ex.getBindingResult();
        String message = "Validation failed. " + validationErrors.getErrorCount() + " error(s)";

        return getProcessedApiErrorResponseForFields(new ApiErrorResponse(message),ex);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ServiceErrorResponseDTO handleNoHandlerFoundError(final NoHandlerFoundException e) {
        return getProcessedApiErrorResponse(new ApiErrorResponse("Invalid URL"));
    }
    
    //added by Hadi to handle 500 ServiceException
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServiceExceptionHolder.ServiceException.class)
    public ServiceErrorResponseDTO handleInternalServerError(final ServiceExceptionHolder.ServiceException ex) {
        ex.printStackTrace();
        System.out.println("ex = " + ex);
        return getProcessedApiErrorResponse(new ApiErrorResponse(ex.getMessage(), ex));
    }

   
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ServiceErrorResponseDTO handleThrowable(final Exception ex) {
        System.out.println("internal server error start");
        ex.printStackTrace();
        System.out.println(" Internal Server Error ");
        return getProcessedApiErrorResponse(new ApiErrorResponse(
                "Internal server error while processing request.",
                ex.getClass().getCanonicalName()
        ));
    }

    private ServiceErrorResponseDTO getProcessedApiErrorResponse(ApiErrorResponse apiErrorResponse) {
        log.error(apiErrorResponse.toString());
        ServiceRequestDTO requestDTO = requestContextComponent.getRequestDTO();

        ServiceErrorResponseHeaderDTO header = requestDTO != null ?
                headerUtilComponent.getErrorResponseHeaderDTO(requestDTO.getHeader()) :
                headerUtilComponent.getResponseHeaderDTO(apiErrorResponse);

        ServiceErrorModel serviceErrorModel = new ServiceErrorModel();
        serviceErrorModel.setGeneralErrors(Collections.singletonList(apiErrorResponse.getMessage()));
        header.setErrors(serviceErrorModel);
        header.setResponseMessage(apiErrorResponse.getMessage());

        ServiceResponseBodyDTO<String> body = new ServiceResponseBodyDTO<>();
        body.setData(apiErrorResponse.getErrors());

        return new ServiceErrorResponseDTO(header, new HashMap<>(), body);
    }

    private ServiceErrorResponseDTO getProcessedApiErrorResponseForFields(ApiErrorResponse apiErrorResponse, MethodArgumentNotValidException ex){

        log.error(apiErrorResponse.toString());
        ServiceRequestDTO requestDTO = requestContextComponent.getRequestDTO();

        ServiceErrorResponseHeaderDTO header = requestDTO != null ?
                headerUtilComponent.getErrorResponseHeaderDTO(requestDTO.getHeader()) :
                headerUtilComponent.getResponseHeaderDTO(apiErrorResponse);

        ServiceErrorModel serviceErrorModel = new ServiceErrorModel();

        HashMap<String, String> errors = new HashMap<>();
        Errors validationErrors = ex.getBindingResult();
        String message = "Validation failed. " + validationErrors.getErrorCount() + " error(s)";

        for (FieldError fieldError : validationErrors.getFieldErrors())
            errors.put(Objects.requireNonNull(fieldError).getField(), fieldError.getDefaultMessage());

        for (ObjectError objectError : validationErrors.getGlobalErrors())
            errors.put("Object " + objectError.getObjectName(), objectError.getDefaultMessage());

        serviceErrorModel.setFieldErrors(errors);
        header.setErrors(serviceErrorModel);
        header.setResponseMessage(apiErrorResponse.getMessage());

        ServiceResponseBodyDTO<String> body = new ServiceResponseBodyDTO<>();
        body.setData(apiErrorResponse.getErrors());

        return new ServiceErrorResponseDTO(header, new HashMap<>(), body);
    }

    // TODO: Conform to API Response Pattern
    @Data
    @Setter(AccessLevel.PRIVATE)
    public class ApiErrorResponse {

        private ServiceErrorResponseHeaderDTO header;
        private final String code;
        private final String message;
        private final List<String> errors;

        private ApiErrorResponse(String message, List<String> errors, ServiceExceptionHolder.ServiceException e) {
            this.code = serviceConfiguration.getShortCode() + e.getCode();
            this.message = message;
            this.errors = errors;
        }

        private ApiErrorResponse(String message, ServiceExceptionHolder.ServiceException e) {
            this(message, new ArrayList<>(), e);
        }

        private ApiErrorResponse(String message, List<String> errors) {
            this(message, errors, new ServiceExceptionHolder.ServiceException(400, message));
        }

        private ApiErrorResponse(String message) {
            this(message, Collections.singletonList(message));
        }

        private ApiErrorResponse(String message, String errorMessage) {
            this(message, Collections.singletonList(errorMessage));
        }
    }
}
