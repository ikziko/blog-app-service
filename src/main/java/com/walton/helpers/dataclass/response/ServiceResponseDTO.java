package com.walton.helpers.dataclass.response;

import com.walton.helpers.models.MetaModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponseDTO<T> {

    private ServiceResponseHeaderDTO header;

    private MetaModel meta;

    private ServiceResponseBodyDTO<T> body;

}