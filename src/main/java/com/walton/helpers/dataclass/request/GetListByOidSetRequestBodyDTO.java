package com.walton.helpers.dataclass.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetListByOidSetRequestBodyDTO implements IRequestBodyDTO {

    public GetListByOidSetRequestBodyDTO(Set<String> oids){
        this.setOids(oids);
    }

    @NotNull
    private Set<@NotBlank String> oids;

    private String strict;

}