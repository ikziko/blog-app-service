package com.walton.helpers.dataclass.request;

import com.walton.helpers.models.MetaModel;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class ServiceRequestDTO<T> {

    @NotNull
    @Valid
    private ServiceRequestHeaderDTO header;

    @NotNull
    @Valid
    private MetaModel meta;

    @NotNull
    @Valid
    private ServiceRequestBodyDTO<T> body;

}