package com.walton.helpers.dataclass.request;

public interface IOidHolderRequestBodyDTO extends IRequestBodyDTO {

    String getOid();

    void setOid(String oid);

}
