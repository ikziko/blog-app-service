package com.walton.helpers.dataclass.request;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class ServiceRequestBodyDTO<T> {

    @NotNull
    @Valid
    List<T> data;

}