package com.walton.helpers.dataclass.common;


import com.walton.helpers.dataclass.request.IRequestBodyDTO;
import lombok.Data;

@Data
public class EmptyBodyDTO implements IRequestBodyDTO {
}