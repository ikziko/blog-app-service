package com.walton.helpers.dataclass.common;

import lombok.Data;

import java.util.ArrayList;

@Data
public class HeaderDTO {

    private String userOid;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private Boolean isAdmin;
    private String jti;
    private String exp;
    private String client_id;
    private ArrayList<String> scope;
    private String token;

}