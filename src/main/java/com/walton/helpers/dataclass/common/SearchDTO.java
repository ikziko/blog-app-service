package com.walton.helpers.dataclass.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchDTO {

    private String oid;
    private Boolean isAdmin;
    private List<String> statusIn;
    private List<String> statusNotIn;

}
