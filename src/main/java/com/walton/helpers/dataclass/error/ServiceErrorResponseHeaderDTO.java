package com.walton.helpers.dataclass.error;

import com.walton.helpers.dataclass.response.ServiceResponseHeaderDTO;
import lombok.Data;

@Data
public class ServiceErrorResponseHeaderDTO extends ServiceResponseHeaderDTO {

    private ServiceErrorModel errors;

}
