package com.walton.helpers.dataclass.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.HashMap;
import java.util.List;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceErrorModel {

    private List<String> generalErrors;
    private HashMap<String, String> fieldErrors;
}
