package com.walton.helpers.dataclass.error;

import com.walton.helpers.dataclass.response.ServiceResponseBodyDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceErrorResponseDTO {

    private ServiceErrorResponseHeaderDTO header;

    private Map<String,Object> meta;

    private ServiceResponseBodyDTO<String> body;

    public ServiceErrorResponseDTO(ServiceErrorResponseHeaderDTO header, HashMap<String,Object> map,ServiceResponseBodyDTO<String> body){
        this.setHeader(header);
        this.setMeta(map);
        this.setBody(body);
    }

}