package com.walton.blog.user.services;

import com.walton.base.services.BaseService;
import com.walton.blog.user.dtos.UserActivationDTO;
import com.walton.blog.user.dtos.UserDTO;
import com.walton.blog.user.entities.User;
import com.walton.blog.user.repositories.UserRepository;
import com.walton.blog.userrole.services.UserRoleService;
import com.walton.helpers.components.EventManagementComponent;
import com.walton.helpers.components.HeaderUtilComponent;
import com.walton.helpers.constants.Constants;
import com.walton.helpers.constants.CrudEvent;
import com.walton.helpers.dataclass.common.HeaderDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import com.walton.helpers.exceptions.ServiceExceptionHolder;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserService extends BaseService<User, UserDTO> {

    private final HeaderUtilComponent headerUtilComponent;
    private final EventManagementComponent eventManagementComponent;
    private final ModelMapper modelMapper;
    private final UserRepository repository;

    public UserService(HeaderUtilComponent headerUtilComponent,
                       UserRepository repository,
                       ModelMapper modelMapper,
                       EventManagementComponent eventManagementComponent) {
        super(headerUtilComponent, repository, modelMapper, eventManagementComponent);
        this.headerUtilComponent = headerUtilComponent;
        this.repository = repository;
        this.eventManagementComponent = eventManagementComponent;
        this.modelMapper = modelMapper;
    }

    @Autowired
    UserRoleService userRoleService;

    @Override
    protected UserDTO convertForRead(User e) {
        UserDTO dto = new UserDTO();
        BeanUtils.copyProperties(e, dto);
        dto.setPassword(null);
        dto.setRoles(userRoleService.getListOfRolesByUser(e));
        return dto;
    }

    @Override
    public List<UserDTO> createAll(List<UserDTO> dtos, HeaderDTO headerDTO) {
        if (!headerDTO.getIsAdmin()) throw new ServiceExceptionHolder.BadRequestException("Sorry, you need admin permission to create a new account !!");
        List<User> entities = new ArrayList<>();
        User e;
        for (UserDTO dto : dtos) {
            e = convertForCreate(dto);
            e.setCreatedBy(headerDTO.getUserOid());
            e.setCreatedOn(new Date());
            e.setIsActive(Constants.YES);
            e.setIsDeleted(Constants.NO);
            entities.add(e);
        }
        List<User> createdEntities = repository.saveAll(entities);
        postCreateAll(dtos, createdEntities);
        List<String> oids = createdEntities.stream().map(o -> o.getOid()).collect(Collectors.toList());
        eventManagementComponent.publishCrudEventBulk(Pair.of(User.class, CrudEvent.CREATE), oids);
        return createdEntities.stream().map(o -> convertForRead(o)).collect(Collectors.toList());
    }

    public UserDTO getUserByEmailAddress(String emailAddress) {
        Optional<User> result = repository.findFirstByEmailAndIsDeleted(emailAddress, Constants.NO);
        if (result.isPresent()) return convertForRead(result.get());
        return null;
    }

    public ServiceResponseDTO<UserDTO> activateOrDeactivateUsers(ServiceRequestDTO<UserActivationDTO> requestDTO, HeaderDTO headerDTO) {
        return generateResponse(requestDTO.getHeader(), activateOrDeactivateUsers(requestDTO.getBody().getData(), headerDTO));
    }

    private List<UserDTO> activateOrDeactivateUsers(List<UserActivationDTO> list, HeaderDTO headerDTO){
        if (!headerDTO.getIsAdmin()) throw new ServiceExceptionHolder.BadRequestException("Sorry, you need admin permission to create a new account !!");
        Map<String, String> map = list.stream().collect(Collectors.toMap(UserActivationDTO::getOid, UserActivationDTO::getIsActive));
        List<User> userList = repository.findByOidInAndIsDeleted(list.stream().map(UserActivationDTO::getOid).collect(Collectors.toSet()), Constants.NO);
        userList = userList.stream().map(user -> {
            user.setIsActive(map.get(user.getOid()));
            return user;
        }).collect(Collectors.toList());
        List<User> updatedEntities = updateAllEntity(userList, headerDTO);
        return convertForRead(updatedEntities);
    }

}
