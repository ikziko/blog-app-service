package com.walton.blog.user.controllers;

import com.walton.base.controllers.BaseController;
import com.walton.blog.user.dtos.UserActivationDTO;
import com.walton.blog.user.dtos.UserDTO;
import com.walton.blog.user.services.UserService;
import com.walton.helpers.constants.Api;
import com.walton.helpers.dataclass.common.EmptyBodyDTO;
import com.walton.helpers.dataclass.request.GetListByOidSetRequestBodyDTO;
import com.walton.helpers.dataclass.request.OidHolderRequestBodyDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = Api.ENDPOINT_USERS)
public class UserController extends BaseController {

    private final UserService service;

    @PostMapping(Api.GET_LIST_PATH)
    public ServiceResponseDTO<UserDTO>
    getList(@Valid @RequestBody ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_USERS + Api.GET_LIST_PATH, requestDTO, service::getList);
    }

    @PostMapping(Api.GET_BY_OID_PATH)
    public ServiceResponseDTO<UserDTO>
    getByOid(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_USERS + Api.GET_BY_OID_PATH, requestDTO, service::getByOid);
    }

    @PostMapping(Api.GET_LIST_BY_OID_SET_PATH)
    public ServiceResponseDTO<UserDTO>
    getListByIdSet(@Valid @RequestBody ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_USERS + Api.GET_LIST_BY_OID_SET_PATH, requestDTO, service::getListByOidSet);
    }

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(Api.CREATE_ALL_PATH)
    public ServiceResponseDTO<UserDTO>
    create(@Valid @RequestBody ServiceRequestDTO<UserDTO> requestDTO) {
        return send(Api.ENDPOINT_USERS + Api.CREATE_PATH, requestDTO, service::createAll);
    }

    @Transactional
    @PostMapping(Api.UPDATE_ALL_PATH)
    public ServiceResponseDTO<UserDTO>
    update(@Valid @RequestBody ServiceRequestDTO<UserDTO> requestDTO) {
        return send(Api.ENDPOINT_USERS + Api.UPDATE_PATH, requestDTO, service::updateAll);
    }

    @Transactional
    @PostMapping(Api.DELETE_ALL_PATH)
    public ServiceResponseDTO<UserDTO>
    deleteAll(@Valid @RequestBody ServiceRequestDTO<String> requestDTO) {
        return send(Api.ENDPOINT_USERS + Api.DELETE_ALL_PATH, requestDTO, service::deleteAll);
    }

    @Transactional
    @PostMapping(Api.ACTIVATE_OR_DEACTIVATE)
    public ServiceResponseDTO<UserDTO>
    activateOrDeactivateUsers(@Valid @RequestBody ServiceRequestDTO<UserActivationDTO> requestDTO) {
        return send(Api.ENDPOINT_USERS + Api.ACTIVATE_OR_DEACTIVATE, requestDTO, service::activateOrDeactivateUsers);
    }


}

