package com.walton.blog.user.repositories;

import com.walton.base.repositories.ServiceRepository;
import com.walton.blog.user.entities.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends ServiceRepository<User> {
    Optional<User> findFirstByEmailAndIsDeleted(String email, String isDeleted);
}