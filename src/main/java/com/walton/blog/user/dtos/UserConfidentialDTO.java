package com.walton.blog.user.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.user.entities.User;
import com.walton.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import com.walton.helpers.validators.annotations.ValidEntityOid;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserConfidentialDTO implements IOidHolderRequestBodyDTO {

    @ValidEntityOid(User.class)
    private String oid;
    private String firstName;
    private String lastName;
    private String email;
    private String isActive;

}
