package com.walton.blog.user.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.walton.base.entities.BaseEntity;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.role.entities.Role;
import com.walton.blog.user.entities.User;
import com.walton.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import com.walton.helpers.validators.annotations.ValidEntityOid;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO implements IOidHolderRequestBodyDTO {

    @ValidEntityOid(User.class)
    private String oid;

    @NotNull(message = "First name must be provided.")
    private String firstName;

    @NotNull(message = "Last name must be provided.")
    private String lastName;

    @NotNull(message = "Email address must be provided.")
    private String email;

    @NotNull(message = "Password must be provided.")
    private String password;

    private String isActive;

    private List<RoleDTO> roles;

}
