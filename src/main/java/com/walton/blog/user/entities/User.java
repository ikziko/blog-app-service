package com.walton.blog.user.entities;

import com.walton.base.entities.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @NotNull(message = "First name must be provided.")
    private String firstName;

    @NotNull(message = "Last name must be provided.")
    private String lastName;

    @NotNull(message = "Email address must be provided.")
    private String email;

    @NotNull(message = "Password must be provided.")
    private String password;

    @Column(columnDefinition = "varchar(5) default 'Yes'")
    private String isActive;

}
