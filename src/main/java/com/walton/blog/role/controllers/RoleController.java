package com.walton.blog.role.controllers;

import com.walton.base.controllers.BaseController;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.role.services.RoleService;
import com.walton.helpers.constants.Api;
import com.walton.helpers.dataclass.common.EmptyBodyDTO;
import com.walton.helpers.dataclass.request.GetListByOidSetRequestBodyDTO;
import com.walton.helpers.dataclass.request.OidHolderRequestBodyDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = Api.ENDPOINT_ROLE)
public class RoleController extends BaseController {

    private final RoleService service;

    @PostMapping(Api.GET_LIST_PATH)
    public ServiceResponseDTO<RoleDTO>
    getList(@Valid @RequestBody ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_ROLE + Api.GET_LIST_PATH, requestDTO, service::getList);
    }

    @PostMapping(Api.GET_BY_OID_PATH)
    public ServiceResponseDTO<RoleDTO>
    getByOid(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_ROLE + Api.GET_BY_OID_PATH, requestDTO, service::getByOid);
    }

    @PostMapping(Api.GET_LIST_BY_OID_SET_PATH)
    public ServiceResponseDTO<RoleDTO>
    getListByIdSet(@Valid @RequestBody ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_ROLE + Api.GET_LIST_BY_OID_SET_PATH, requestDTO, service::getListByOidSet);
    }

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(Api.CREATE_ALL_PATH)
    public ServiceResponseDTO<RoleDTO>
    create(@Valid @RequestBody ServiceRequestDTO<RoleDTO> requestDTO) {
        return send(Api.ENDPOINT_ROLE + Api.CREATE_PATH, requestDTO, service::createAll);
    }

    @Transactional
    @PostMapping(Api.UPDATE_ALL_PATH)
    public ServiceResponseDTO<RoleDTO>
    update(@Valid @RequestBody ServiceRequestDTO<RoleDTO> requestDTO) {
        return send(Api.ENDPOINT_ROLE + Api.UPDATE_PATH, requestDTO, service::updateAll);
    }

    @Transactional
    @PostMapping(Api.DELETE_ALL_PATH)
    public ServiceResponseDTO<RoleDTO>
    deleteAll(@Valid @RequestBody ServiceRequestDTO<String> requestDTO) {
        return send(Api.ENDPOINT_ROLE + Api.DELETE_ALL_PATH, requestDTO, service::deleteAll);
    }


}

