package com.walton.blog.role.entities;

import com.walton.base.entities.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "roles")
public class Role extends BaseEntity {

    @NotNull(message = "Role title must be provided.")
    private String title;

    @NotNull(message = "Role tag must be provided.")
    private String tag;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

}
