package com.walton.blog.role.services;

import com.walton.base.services.BaseService;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.role.entities.Role;
import com.walton.blog.role.repositories.RoleRepository;
import com.walton.helpers.components.EventManagementComponent;
import com.walton.helpers.components.HeaderUtilComponent;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RoleService extends BaseService<Role, RoleDTO> {

    private final HeaderUtilComponent headerUtilComponent;
    private final EventManagementComponent eventManagementComponent;
    private final ModelMapper modelMapper;
    private final RoleRepository repository;

    public RoleService(HeaderUtilComponent headerUtilComponent,
                       RoleRepository repository,
                       ModelMapper modelMapper,
                       EventManagementComponent eventManagementComponent) {
        super(headerUtilComponent, repository, modelMapper, eventManagementComponent);
        this.headerUtilComponent = headerUtilComponent;
        this.repository = repository;
        this.eventManagementComponent = eventManagementComponent;
        this.modelMapper = modelMapper;
    }


}
