package com.walton.blog.role.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.walton.blog.role.entities.Role;
import com.walton.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import com.walton.helpers.validators.annotations.ValidEntityOid;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * @author imtiaz
 * @created 23/12/2021 - 9:10 PM
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoleDTO implements IOidHolderRequestBodyDTO {

    @ValidEntityOid(Role.class)
    private String oid;

    @NotNull(message = "Role title must be provided.")
    private String title;

    @NotNull(message = "Role tag must be provided.")
    private String tag;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;
}
