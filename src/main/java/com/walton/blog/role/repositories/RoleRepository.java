package com.walton.blog.role.repositories;

import com.walton.base.repositories.ServiceRepository;
import com.walton.blog.role.entities.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends ServiceRepository<Role> {
}