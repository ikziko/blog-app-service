package com.walton.blog.blogpost.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.walton.blog.blogpost.entities.BlogPost;
import com.walton.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import com.walton.helpers.validators.annotations.ValidEntityOid;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlogPostDTO implements IOidHolderRequestBodyDTO {

    @ValidEntityOid(BlogPost.class)
    private String oid;

    @NotNull(message = "Post title must be provided.")
    private String title;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    private String status;

    List<BlogPostCommentShortDTO> comments;

}
