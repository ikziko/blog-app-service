package com.walton.blog.blogpost.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.walton.blog.blogpost.entities.BlogPostComment;
import com.walton.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import com.walton.helpers.validators.annotations.ValidEntityOid;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlogPostCommentShortDTO implements IOidHolderRequestBodyDTO {

    @ValidEntityOid(BlogPostComment.class)
    private String oid;

    private String comment;


}
