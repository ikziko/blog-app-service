package com.walton.blog.blogpost.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.walton.blog.blogpost.entities.BlogPost;
import com.walton.blog.blogpost.entities.BlogPostComment;
import com.walton.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import com.walton.helpers.validators.annotations.ValidEntityOid;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlogPostCommentDTO implements IOidHolderRequestBodyDTO {

    @ValidEntityOid(BlogPostComment.class)
    private String oid;

    @NotBlank(message = "Post oid must be provided.")
    private String blogPostOid;

    @NotBlank(message = "Comment must be provided.")
    private String comment;

    private BlogPostDTO blogPost;

}
