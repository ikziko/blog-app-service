package com.walton.blog.blogpost.controllers;

import com.walton.base.controllers.BaseController;
import com.walton.blog.blogpost.dtos.BlogPostCommentDTO;
import com.walton.blog.blogpost.services.BlogPostCommentService;
import com.walton.helpers.constants.Api;
import com.walton.helpers.dataclass.common.EmptyBodyDTO;
import com.walton.helpers.dataclass.request.GetListByOidSetRequestBodyDTO;
import com.walton.helpers.dataclass.request.OidHolderRequestBodyDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = Api.ENDPOINT_BLOG_POST_COMMENT)
public class BlogPostCommentController extends BaseController {

    private final BlogPostCommentService service;

    @PostMapping(Api.GET_BY_OID_PATH)
    public ServiceResponseDTO<BlogPostCommentDTO>
    getByOid(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST_COMMENT + Api.GET_BY_OID_PATH, requestDTO, service::getByOid);
    }

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(Api.CREATE_ALL_PATH)
    public ServiceResponseDTO<BlogPostCommentDTO>
    create(@Valid @RequestBody ServiceRequestDTO<BlogPostCommentDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST_COMMENT + Api.CREATE_PATH, requestDTO, service::createAll);
    }

    @Transactional
    @PostMapping(Api.UPDATE_ALL_PATH)
    public ServiceResponseDTO<BlogPostCommentDTO>
    update(@Valid @RequestBody ServiceRequestDTO<BlogPostCommentDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST_COMMENT + Api.UPDATE_PATH, requestDTO, service::updateAll);
    }

    @Transactional
    @PostMapping(Api.DELETE_ALL_PATH)
    public ServiceResponseDTO<BlogPostCommentDTO>
    deleteAll(@Valid @RequestBody ServiceRequestDTO<String> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST_COMMENT + Api.DELETE_ALL_PATH, requestDTO, service::deleteAll);
    }


}

