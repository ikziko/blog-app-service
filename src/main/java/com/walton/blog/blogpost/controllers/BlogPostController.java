package com.walton.blog.blogpost.controllers;

import com.walton.base.controllers.BaseController;
import com.walton.blog.blogpost.dtos.BlogPostDTO;
import com.walton.blog.blogpost.services.BlogPostService;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.role.services.RoleService;
import com.walton.helpers.constants.Api;
import com.walton.helpers.dataclass.common.EmptyBodyDTO;
import com.walton.helpers.dataclass.request.GetListByOidSetRequestBodyDTO;
import com.walton.helpers.dataclass.request.OidHolderRequestBodyDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = Api.ENDPOINT_BLOG_POST)
public class BlogPostController extends BaseController {

    private final BlogPostService service;

    @PostMapping(Api.GET_DRAFTED_POST_LIST_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    getMyDraftedPostList(@Valid @RequestBody ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.GET_DRAFTED_POST_LIST_PATH, requestDTO, service::getMyDraftedPostList);
    }

    @PostMapping(Api.GET_PUBLISHED_POST_LIST_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    getPublishedPostList(@Valid @RequestBody ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.GET_PUBLISHED_POST_LIST_PATH, requestDTO, service::getPublishedPostList);
    }

    @PostMapping(Api.GET_APPROVED_POST_LIST_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    getApprovedPostList(@Valid @RequestBody ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.GET_APPROVED_POST_LIST_PATH, requestDTO, service::getApprovedPostList);
    }

    @PostMapping(Api.GET_BY_OID_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    getByOid(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.GET_BY_OID_PATH, requestDTO, service::getByOid);
    }

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(Api.CREATE_ALL_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    create(@Valid @RequestBody ServiceRequestDTO<BlogPostDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.CREATE_PATH, requestDTO, service::createAll);
    }

    @Transactional
    @PostMapping(Api.UPDATE_ALL_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    update(@Valid @RequestBody ServiceRequestDTO<BlogPostDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.UPDATE_PATH, requestDTO, service::updateAll);
    }

    @Transactional
    @PostMapping(Api.DELETE_ALL_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    deleteAll(@Valid @RequestBody ServiceRequestDTO<String> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.DELETE_ALL_PATH, requestDTO, service::deleteAll);
    }

    @PostMapping(Api.PUBLISH_POST_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    publishMyPost(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.PUBLISH_POST_PATH, requestDTO, service::publishMyPost);
    }

    @PostMapping(Api.APPROVE_POST_PATH)
    public ServiceResponseDTO<BlogPostDTO>
    approvePost(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_BLOG_POST + Api.APPROVE_POST_PATH, requestDTO, service::approvePost);
    }


}

