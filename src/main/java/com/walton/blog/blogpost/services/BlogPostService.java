package com.walton.blog.blogpost.services;

import com.walton.base.services.BaseService;
import com.walton.blog.blogpost.dtos.BlogPostCommentDTO;
import com.walton.blog.blogpost.dtos.BlogPostCommentShortDTO;
import com.walton.blog.blogpost.dtos.BlogPostDTO;
import com.walton.blog.blogpost.entities.BlogPost;
import com.walton.blog.blogpost.repositories.BlogPostRepository;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.role.entities.Role;
import com.walton.blog.role.repositories.RoleRepository;
import com.walton.helpers.components.EventManagementComponent;
import com.walton.helpers.components.HeaderUtilComponent;
import com.walton.helpers.constants.Constants;
import com.walton.helpers.dataclass.common.EmptyBodyDTO;
import com.walton.helpers.dataclass.common.HeaderDTO;
import com.walton.helpers.dataclass.request.OidHolderRequestBodyDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import com.walton.helpers.exceptions.ServiceExceptionHolder;
import com.walton.util.UtilCharacter;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BlogPostService extends BaseService<BlogPost, BlogPostDTO> {

    private final HeaderUtilComponent headerUtilComponent;
    private final EventManagementComponent eventManagementComponent;
    private final ModelMapper modelMapper;
    private final BlogPostRepository repository;

    public BlogPostService(HeaderUtilComponent headerUtilComponent,
                           BlogPostRepository repository,
                           ModelMapper modelMapper,
                           EventManagementComponent eventManagementComponent) {
        super(headerUtilComponent, repository, modelMapper, eventManagementComponent);
        this.headerUtilComponent = headerUtilComponent;
        this.repository = repository;
        this.eventManagementComponent = eventManagementComponent;
        this.modelMapper = modelMapper;
    }

    @Autowired
    UtilCharacter utilCharacter;

    @Autowired
    BlogPostCommentService commentService;

    @Override
    protected BlogPost convertForCreate(BlogPostDTO d) {
        BlogPost e = null;
        try {
            e = BlogPost.class.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
        BeanUtils.copyProperties(d, e);
        e.setStatus(Constants.STATUS_DRAFT);
        return e;
    }

    @Override
    protected BlogPostDTO convertForRead(BlogPost e) {
        BlogPostDTO blogPostDTO = modelMapper.map(e, BlogPostDTO.class);
        if (!utilCharacter.noData(e.getStatus()) && e.getStatus().equals(Constants.STATUS_APPROVED)) {
            List<BlogPostCommentShortDTO> comments = commentService.getAllCommentsByBlogPost(e);
            blogPostDTO.setComments(comments);
        }
        return blogPostDTO;
    }

    public ServiceResponseDTO<BlogPostDTO> getMyDraftedPostList(ServiceRequestDTO<EmptyBodyDTO> requestDTO, HeaderDTO headerDTO) {
        List<BlogPost> postList = repository.findByStatusAndCreatedByAndIsDeleted(Constants.STATUS_DRAFT, headerDTO.getUserOid(), Constants.NO);
        return generateResponse(requestDTO.getHeader(), convertForRead(postList));
    }

    public ServiceResponseDTO<BlogPostDTO> getPublishedPostList(ServiceRequestDTO<EmptyBodyDTO> requestDTO, HeaderDTO headerDTO) {
        List<BlogPost> postList = new ArrayList<>();
        if(headerDTO.getIsAdmin()) postList = repository.findByStatusAndIsDeleted(Constants.STATUS_PUBLISHED, Constants.NO);
        else postList = repository.findByStatusAndCreatedByAndIsDeleted(Constants.STATUS_PUBLISHED, headerDTO.getUserOid(), Constants.NO);
        return generateResponse(requestDTO.getHeader(), convertForRead(postList));
    }

    public ServiceResponseDTO<BlogPostDTO> getApprovedPostList(ServiceRequestDTO<EmptyBodyDTO> requestDTO, HeaderDTO headerDTO) {
        List<BlogPost> postList = repository.findByStatusAndIsDeleted(Constants.STATUS_APPROVED, Constants.NO);
        return generateResponse(requestDTO.getHeader(), convertForRead(postList));
    }

    public ServiceResponseDTO<BlogPostDTO> publishMyPost(ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO, HeaderDTO headerDTO) {
        Optional<BlogPost> result = repository.findByOidAndStatusAndCreatedByAndIsDeleted(requestDTO.getBody().getData().get(0).getOid(), Constants.STATUS_DRAFT, headerDTO.getUserOid(), Constants.NO);
        if (!result.isPresent()) throw new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException("Sorry, publish this post is not allowed!!");
        BlogPost blogPost = result.get();
        blogPost.setStatus(Constants.STATUS_PUBLISHED);
        List<BlogPost> postList = updateAllEntity(Collections.singletonList(blogPost), headerDTO);
        return generateResponse(requestDTO.getHeader(), convertForRead(postList));
    }

    public ServiceResponseDTO<BlogPostDTO> approvePost(ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO, HeaderDTO headerDTO) {
        if(!headerDTO.getIsAdmin()) throw new ServiceExceptionHolder.BadRequestException("Sorry, you are not authorized to approve this post.");
        Optional<BlogPost> result = repository.findByOidAndStatusAndIsDeleted(requestDTO.getBody().getData().get(0).getOid(), Constants.STATUS_PUBLISHED, Constants.NO);
        if (!result.isPresent()) throw new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException("Sorry, this post shouldn't be approved!!");
        BlogPost blogPost = result.get();
        blogPost.setStatus(Constants.STATUS_APPROVED);
        List<BlogPost> postList = updateAllEntity(Collections.singletonList(blogPost), headerDTO);
        return generateResponse(requestDTO.getHeader(), convertForRead(postList));
    }

    @Override
    public List<BlogPostDTO> deleteAll(Set<String> oidSet, HeaderDTO headerDTO) {
        List<BlogPost> postEntities = repository.findByOidInAndIsDeleted(oidSet, Constants.NO);
        if (!headerDTO.getIsAdmin()) {
            // check all posts are mine
            postEntities.stream().forEach(post -> {
                if (!post.getCreatedBy().equals(headerDTO.getUserOid()))
                    throw new ServiceExceptionHolder.BadRequestException("Sorry, you are not authorized to delete post.");
            });
        }
        List<BlogPostDTO> deletedEntities = postEntities.stream().map(post -> deleteEntity(post, headerDTO)).collect(Collectors.toList());
        return deletedEntities;
    }
}
