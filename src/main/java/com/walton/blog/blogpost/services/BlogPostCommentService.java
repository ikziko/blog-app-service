package com.walton.blog.blogpost.services;

import com.walton.base.services.BaseService;
import com.walton.blog.blogpost.dtos.BlogPostCommentDTO;
import com.walton.blog.blogpost.dtos.BlogPostCommentShortDTO;
import com.walton.blog.blogpost.dtos.BlogPostDTO;
import com.walton.blog.blogpost.entities.BlogPost;
import com.walton.blog.blogpost.entities.BlogPostComment;
import com.walton.blog.blogpost.repositories.BlogPostCommentRepository;
import com.walton.blog.blogpost.repositories.BlogPostRepository;
import com.walton.helpers.components.EventManagementComponent;
import com.walton.helpers.components.HeaderUtilComponent;
import com.walton.helpers.constants.Constants;
import com.walton.helpers.exceptions.ServiceExceptionHolder;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BlogPostCommentService extends BaseService<BlogPostComment, BlogPostCommentDTO> {

    private final HeaderUtilComponent headerUtilComponent;
    private final EventManagementComponent eventManagementComponent;
    private final ModelMapper modelMapper;
    private final BlogPostCommentRepository repository;

    public BlogPostCommentService(HeaderUtilComponent headerUtilComponent,
                                  BlogPostCommentRepository repository,
                                  ModelMapper modelMapper,
                                  EventManagementComponent eventManagementComponent) {
        super(headerUtilComponent, repository, modelMapper, eventManagementComponent);
        this.headerUtilComponent = headerUtilComponent;
        this.repository = repository;
        this.eventManagementComponent = eventManagementComponent;
        this.modelMapper = modelMapper;
    }

    @Autowired
    BlogPostService blogPostService;

    @Override
    protected BlogPostComment convertForCreate(BlogPostCommentDTO d) {
        BlogPostComment e = null;
        try {
            e = BlogPostComment.class.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
        BeanUtils.copyProperties(d, e);
        putBlogPost(d, e);
        return e;
    }

    @Override
    protected BlogPostComment convertForUpdate(BlogPostCommentDTO d, BlogPostComment e) {
        BeanUtils.copyProperties(d, e);
        putBlogPost(d, e);
        return e;
    }

    private void putBlogPost(BlogPostCommentDTO d, BlogPostComment e) {
        Optional<BlogPost> result = blogPostService.getOptionalEntity(d.getBlogPostOid());
        if (result.isPresent() && result.get().getStatus().equals(Constants.STATUS_APPROVED)) e.setBlogPost(result.get());
        else throw new ServiceExceptionHolder.ResourceNotFoundDuringWriteRequestException("Sorry, can't add comment on this post right now.");
    }

    public List<BlogPostCommentShortDTO> getAllCommentsByBlogPost(BlogPost blogPost) {
        List<BlogPostComment> comments = repository.findByBlogPostAndIsDeletedOrderByCreatedOnDesc(blogPost, Constants.NO);
        return comments.stream().map(comment -> modelMapper.map(comment, BlogPostCommentShortDTO.class)).collect(Collectors.toList());
    }


}
