package com.walton.blog.blogpost.entities;

import com.walton.base.entities.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "blog_post")
public class BlogPost extends BaseEntity {

    @NotNull(message = "Post title must be provided.")
    private String title;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(columnDefinition = "varchar(20) default 'draft'")
    private String status;

}
