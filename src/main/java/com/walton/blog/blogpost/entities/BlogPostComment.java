package com.walton.blog.blogpost.entities;

import com.walton.base.entities.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "blog_post_comments")
public class BlogPostComment extends BaseEntity {

    @NotNull(message = "Post must be provided.")
    @JoinColumn(name = "blog_post_oid")
    @ManyToOne
    private BlogPost blogPost;

    @NotBlank(message = "Comment must be provided.")
    @Column(columnDefinition = "TEXT")
    private String comment;

}
