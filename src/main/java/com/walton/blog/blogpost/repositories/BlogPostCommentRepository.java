package com.walton.blog.blogpost.repositories;

import com.walton.base.repositories.ServiceRepository;
import com.walton.blog.blogpost.entities.BlogPost;
import com.walton.blog.blogpost.entities.BlogPostComment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlogPostCommentRepository extends ServiceRepository<BlogPostComment> {
    List<BlogPostComment> findByBlogPostAndIsDeletedOrderByCreatedOnDesc(BlogPost blogPost, String isDeleted);
}