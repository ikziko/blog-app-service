package com.walton.blog.blogpost.repositories;

import com.walton.base.repositories.ServiceRepository;
import com.walton.blog.blogpost.entities.BlogPost;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface BlogPostRepository extends ServiceRepository<BlogPost> {
    List<BlogPost> findByOidInAndIsDeleted(Set<String> oidSet, String isDeleted);
    List<BlogPost> findByStatusAndIsDeleted(String status, String isDeleted);
    List<BlogPost> findByStatusAndCreatedByAndIsDeleted(String status, String createdBy, String isDeleted);
    Optional<BlogPost> findByOidAndStatusAndCreatedByAndIsDeleted(String oid, String status, String createdBy, String isDeleted);
    Optional<BlogPost> findByOidAndStatusAndIsDeleted(String oid, String status, String isDeleted);
}