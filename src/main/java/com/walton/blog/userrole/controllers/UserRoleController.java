package com.walton.blog.userrole.controllers;

import com.walton.base.controllers.BaseController;
import com.walton.blog.userrole.dtos.UserRoleDTO;
import com.walton.blog.userrole.services.UserRoleService;
import com.walton.helpers.constants.Api;
import com.walton.helpers.dataclass.common.EmptyBodyDTO;
import com.walton.helpers.dataclass.request.GetListByOidSetRequestBodyDTO;
import com.walton.helpers.dataclass.request.OidHolderRequestBodyDTO;
import com.walton.helpers.dataclass.request.ServiceRequestDTO;
import com.walton.helpers.dataclass.response.ServiceResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = Api.ENDPOINT_USER_ROLE)
public class UserRoleController extends BaseController {

    private final UserRoleService service;

    @PostMapping(Api.GET_LIST_PATH)
    public ServiceResponseDTO<UserRoleDTO>
    getList(@Valid @RequestBody ServiceRequestDTO<EmptyBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_USER_ROLE + Api.GET_LIST_PATH, requestDTO, service::getList);
    }

    @PostMapping(Api.GET_BY_OID_PATH)
    public ServiceResponseDTO<UserRoleDTO>
    getByOid(@Valid @RequestBody ServiceRequestDTO<OidHolderRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_USER_ROLE + Api.GET_BY_OID_PATH, requestDTO, service::getByOid);
    }

    @PostMapping(Api.GET_LIST_BY_OID_SET_PATH)
    public ServiceResponseDTO<UserRoleDTO>
    getListByIdSet(@Valid @RequestBody ServiceRequestDTO<GetListByOidSetRequestBodyDTO> requestDTO) {
        return send(Api.ENDPOINT_USER_ROLE + Api.GET_LIST_BY_OID_SET_PATH, requestDTO, service::getListByOidSet);
    }

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(Api.CREATE_ALL_PATH)
    public ServiceResponseDTO<UserRoleDTO>
    create(@Valid @RequestBody ServiceRequestDTO<UserRoleDTO> requestDTO) {
        return send(Api.ENDPOINT_USER_ROLE + Api.CREATE_PATH, requestDTO, service::createAll);
    }

    @Transactional
    @PostMapping(Api.UPDATE_ALL_PATH)
    public ServiceResponseDTO<UserRoleDTO>
    update(@Valid @RequestBody ServiceRequestDTO<UserRoleDTO> requestDTO) {
        return send(Api.ENDPOINT_USER_ROLE + Api.UPDATE_PATH, requestDTO, service::updateAll);
    }

    @Transactional
    @PostMapping(Api.DELETE_ALL_PATH)
    public ServiceResponseDTO<UserRoleDTO>
    deleteAll(@Valid @RequestBody ServiceRequestDTO<String> requestDTO) {
        return send(Api.ENDPOINT_USER_ROLE + Api.DELETE_ALL_PATH, requestDTO, service::deleteAll);
    }


}

