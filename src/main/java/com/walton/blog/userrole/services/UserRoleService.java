package com.walton.blog.userrole.services;

import com.walton.base.services.BaseService;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.role.entities.Role;
import com.walton.blog.role.services.RoleService;
import com.walton.blog.user.entities.User;
import com.walton.blog.userrole.dtos.UserRoleDTO;
import com.walton.blog.userrole.entities.UserRole;
import com.walton.blog.userrole.repositories.UserRoleRepository;
import com.walton.helpers.components.EventManagementComponent;
import com.walton.helpers.components.HeaderUtilComponent;
import com.walton.helpers.constants.Constants;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserRoleService extends BaseService<UserRole, UserRoleDTO> {

    private final HeaderUtilComponent headerUtilComponent;
    private final EventManagementComponent eventManagementComponent;
    private final ModelMapper modelMapper;
    private final UserRoleRepository repository;

    public UserRoleService(HeaderUtilComponent headerUtilComponent,
                           UserRoleRepository repository,
                           ModelMapper modelMapper,
                           EventManagementComponent eventManagementComponent) {
        super(headerUtilComponent, repository, modelMapper, eventManagementComponent);
        this.headerUtilComponent = headerUtilComponent;
        this.repository = repository;
        this.eventManagementComponent = eventManagementComponent;
        this.modelMapper = modelMapper;
    }

    @Autowired
    RoleService roleService;

    @Override
    protected UserRole convertForCreate(UserRoleDTO d) {
        UserRole e = null;
        try {
            e = UserRole.class.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
        BeanUtils.copyProperties(d, e);

        Role role = new Role();
        role.setOid(d.getRoleOid());

        User user = new User();
        user.setOid(d.getUserOid());

        e.setUser(user);
        e.setRole(role);

        return e;
    }

    public List<RoleDTO> getListOfRolesByUser(User user) {
        return repository.findByUserAndIsDeleted(user, Constants.NO).stream().map(userRole -> {
            RoleDTO dto = new RoleDTO();
            BeanUtils.copyProperties(userRole.getRole(), dto);
            return dto;
        }).collect(Collectors.toList());
    }


}
