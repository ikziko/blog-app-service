package com.walton.blog.userrole.entities;

import com.walton.base.entities.BaseEntity;
import com.walton.blog.role.entities.Role;
import com.walton.blog.user.entities.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class UserRole extends BaseEntity {

    @JoinColumn(name = "user_oid")
    @ManyToOne
    @NotNull(message = "User must be selected.")
    private User user;

    @JoinColumn(name = "role_oid")
    @ManyToOne
    @NotNull(message = "Role must be selected.")
    private Role role;

}
