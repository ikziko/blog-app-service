package com.walton.blog.userrole.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.walton.blog.role.dtos.RoleDTO;
import com.walton.blog.user.dtos.UserConfidentialDTO;
import com.walton.blog.userrole.entities.UserRole;
import com.walton.helpers.dataclass.request.IOidHolderRequestBodyDTO;
import com.walton.helpers.validators.annotations.ValidEntityOid;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRoleDTO implements IOidHolderRequestBodyDTO {

    @ValidEntityOid(UserRole.class)
    private String oid;

    @NotBlank(message = "user oid must be provided.")
    private String userOid;

    @NotBlank(message = "role oid must be provided.")
    private String roleOid;

    private UserConfidentialDTO user;

    private RoleDTO role;

}
