package com.walton.blog.userrole.repositories;

import com.walton.base.repositories.ServiceRepository;
import com.walton.blog.user.entities.User;
import com.walton.blog.userrole.entities.UserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends ServiceRepository<UserRole> {
    List<UserRole> findByUserAndIsDeleted(User user, String isDeleted);
}