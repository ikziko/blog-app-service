# README #

### Prerequisite ###
* minimum JAVA version 1.8
* minimum MAVEN version 3.6.3

### Start the project ###
* clone from the git repository
* go to the downloaded project directory
* execute command (mvn clean install) and let maven download the dependencies and build the project
* execute command (mvn spring-boot:run) to run the project
* it will be available at [url](http://localhost:9001/blog-app-service)

### Data-source ###
* H2
* [url](http://localhost:9001/blog-app-service/h2-console)
* username:sa
* password:sa

### Api documentation ###
* [swagger](http://localhost:9001/blog-app-service/swagger-ui.html)

### Generate javadoc ###
* execute command (mvn javadoc:javadoc)
* javadoc will be available at target/site/apidocs/index.html

### User JWT's (Use this while testing api's on postman/others)###

Admin
===========
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyT2lkIjoiYmFhNTdjYmYtMTA3Yy00M2QxLTllMGUtMDVkYTQ0MTVhMGFiIiwidXNlckZpcnN0TmFtZSI6IlN5c3RlbSIsInVzZXJMYXN0TmFtZSI6IlN1cGVyYWRtaW4iLCJ1c2VyRW1haWwiOiJhZG1pbkBlbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlfQ.Dvu2TBzwoZR6uGQ1BJnyl1E56i8fO-PA4VpvptMKYCY

Imtiaz - blogger
=================
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyT2lkIjoiNDU4MDgxNjMtNDgxMC00ODI4LTlmNDItNmEzZDQzZTQxMDUzIiwidXNlckZpcnN0TmFtZSI6IkltdGlheiIsInVzZXJMYXN0TmFtZSI6IkthYmlyIiwidXNlckVtYWlsIjoiaW10aWF6QGVtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlfQ.TZYi0lkNLzlhJIZvnJ97QoYwOVR8ZP7_hwAzld5AXJQ

Mehedi - blogger
=================
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyT2lkIjoiYTI2NTc0ZjUtNjVmNC00NzhjLWJkYTEtYTY3N2Y2N2E1NmUzIiwidXNlckZpcnN0TmFtZSI6Ik1laGVkaSIsInVzZXJMYXN0TmFtZSI6Ikhhc2FuIiwidXNlckVtYWlsIjoibWVoZWRpQGVtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlfQ.XL59xeHBrqwJ_X4J1Oq2iaGGm-fBmJjHTV3udHgyn-w